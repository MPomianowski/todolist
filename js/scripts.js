var add = document.getElementById("dodaj");
var task = document.getElementsByClassName("list-group");
var id = 1;
var dataFromDBid = [];
var dataFromDBname = [];
var dataFromDBdone = [];
var dataFromDBdate = [];


$(document).ready(function() {
    $.ajax({
        url: "http://localhost:8080/rest/tasks/all"
    }).then(function(data) {
        console.log(data);
        for(let i = 0; i < data.length; i++){
            dataFromDBid[i]  = data[i].id;
            dataFromDBname[i]  = data[i].name;
            dataFromDBdone[i]  = data[i].isDone;
            dataFromDBdate[i]  = data[i].creationDate;
        }
        for(let i = 0; i<dataFromDBid.length; i++){
        var taskName = dataFromDBname[i];
        var taskId = dataFromDBid[i];
        var isDone = dataFromDBdone[i];
        createTask(taskName, isDone, taskId);
        }
    });
});


function createTask(taskName, isDone, taskId){
    if(taskName != ''){
    document.getElementById("addTask").value = '';
    var node = document.createElement("div"); 
    var textnode = document.createTextNode(taskName);  
    node.appendChild(textnode);
    node.classList.add("list-group-item");
    if(isDone){
        node.classList.add("done");
    }
    node.setAttribute("id", id);
    node.innerHTML +=  '<button type="button" id="delete'+ id +'" class="btn btn-lg btn-danger">usuń</button>';
    node.addEventListener("click", function(){
        var xhttp = new XMLHttpRequest();
        if(!isDone){
        xhttp.open("POST", "http://localhost:8080/rest/tasks/done", true);
        } else {
        xhttp.open("POST", "http://localhost:8080/rest/tasks/undone", true);
        }
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(taskId);
        setInterval(location.reload(), 1000);
    });
    task[0].appendChild(node);
    var deleteButton = document.getElementById("delete"+id);
    deleteButton.addEventListener("click", function(){
        var xhttp = new XMLHttpRequest();
        xhttp.open("DELETE", "http://localhost:8080/rest/tasks", true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(taskId);
        setInterval(location.reload(), 1000);
    });
    id++;
    }
}

function UserAction() {
    var taskDesc = document.getElementById("addTask").value;
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "http://localhost:8080/rest/tasks/save", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send('{ "name":"'+ taskDesc +'"}');
    setInterval(location.reload(), 1000);
}

function done(id){
    var taskToBeDone = document.getElementById(id);
    taskToBeDone.classList.add("done");
}











